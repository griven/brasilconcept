<?php

namespace App\Message;

interface IMessage {

    public function sayHello();

    public static function getNativeLang();
}