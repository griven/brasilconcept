<?php

namespace App\Message;


use App\Config;

class Factory
{
    public static function getMessage(Config $config) : BaseMessage
    {
        switch ($config->getCountry()) {
            case 'Brasil':
                return new BrasilMessage();
            case 'England':
                return new EnglandMessage();
            default:
                return new BaseMessage();
        }
    }
}