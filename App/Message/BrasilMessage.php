<?php

namespace App\Message;


class BrasilMessage extends BaseMessage
{
    protected static $nativeLang = 'Portuguese';

    public function sayHello()
    {
        echo 'Bem-vindo ao Portuguese ' . $this->getServiceName() . PHP_EOL;
    }

}