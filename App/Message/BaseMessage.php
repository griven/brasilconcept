<?php

namespace App\Message;


class BaseMessage implements IMessage
{
    protected static $serviceName = 'Studybay';
    protected static $nativeLang = 'English';

    public function __construct()
    {
        echo 'Creating class ' . get_class($this) . PHP_EOL;
    }

    public function sayHello()
    {
        echo 'You are welcome on ' . $this->getServiceName() . PHP_EOL;
    }

    public static function getNativeLang()
    {
        return static::$nativeLang;
    }

    protected function getServiceName()
    {
        return static::$serviceName;
    }
}