<?php

namespace App\Message;


class EnglandMessage extends BaseMessage
{
    public function sayHello()
    {
        echo 'You are welcome on ENGLAND ' . $this->getServiceName() . PHP_EOL;
    }

}