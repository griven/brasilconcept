<?php

namespace App\User;

class BrasilUser extends BaseUser
{
    public $motherSurname; // фамилия матери

    public static function findById(int $id)
    {
        return new self($id);
    }

    public function getFullName() {
        return "$this->name $this->motherSurname da $this->surname";
    }

    public function __construct(int $id)
    {
        parent::__construct($id);

        $this->motherSurname = 'Costa';
        $this->surname = 'Silva';
    }
}