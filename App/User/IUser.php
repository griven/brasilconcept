<?php

namespace App\User;

interface IUser {
    public static function findById(int $id);
    public function getFullName();
}