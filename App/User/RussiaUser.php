<?php

namespace App\User;

class RussiaUser extends BaseUser
{
    public $patronymic; // отчество

    public static function findById(int $id)
    {
        return new self($id);
    }

    public function getFullName() {
        return "$this->name $this->patronymic $this->surname";
    }

    public function __construct(int $id)
    {
        parent::__construct($id);

        $this->name = 'Ivan';
        $this->patronymic = 'Ivanovich';
    }
}