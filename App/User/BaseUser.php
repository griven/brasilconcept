<?php

namespace App\User;


use App\Config;

class BaseUser implements IUser
{
    public $id;
    public $name;
    public $surname;

    public static function findById(int $id)
    {
        switch (Config::getInstance()->getCountry()) {
            case 'Brasil':
                return new BrasilUser($id);
            case 'Russia':
                return new RussiaUser($id);
            default:
                return new BaseUser($id);
        }
    }

    public function getFullName() {
        return "Mr. $this->name $this->surname";
    }

    public function __construct(int $id = null)
    {
        $this->id = $id;
        $this->name = 'John';
        $this->surname = 'Doe';
    }
}