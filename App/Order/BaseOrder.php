<?php

namespace App\Order;

use App\User\BaseUser;
use App\User\IUser;

class BaseOrder
{
    public $id;
    public $ownerId;

    public static function findById(int $id) : BaseOrder
    {
        return new BaseOrder($id);
    }

    public function __construct(int $id = null)
    {
        $this->id = $id;
        $this->ownerId = /* получаем из базы */ rand(1, 10);
    }

    public function getInfo()
    {
        $fullName = $this->getOwner()->getFullName();
        return "OrderId: $this->id, OwnerName: $fullName";
    }

    private function getOwner() : IUser
    {
        return BaseUser::findById($this->ownerId);
    }
}