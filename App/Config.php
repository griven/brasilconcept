<?php

namespace App;


class Config
{
    private static $instance;
    private $country;

    public static function getInstance(string $country = 'USA') : Config
    {
        if(empty(self::$instance)) {
            self::$instance = new Config($country);
        }

        return self::$instance;
    }

    public function getCountry() : string
    {
        return $this->country;
    }

    private function __construct(string $country)
    {
        $this->country = $country;
    }
}