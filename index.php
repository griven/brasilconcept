<?php

require __DIR__ . '/vendor/autoload.php';

$country = $argv[1] ?? die('Need to choose country');
$config = \App\Config::getInstance($country);

// Пример 1. Сообщения зависят от переданного аргумента конфигурации
echo '----------------------' . PHP_EOL;
$message = \App\Message\Factory::getMessage($config);
$message->sayHello();
echo $message::getNativeLang() . PHP_EOL;


// Пример 2. Вывод Заказа зависит от синглтона конфигурации, который можно вызвать в любом месте системы
echo '----------------------' . PHP_EOL;
$randomId = rand(10, 100);
$order = \App\Order\BaseOrder::findById($randomId);
echo $order->getInfo() . PHP_EOL;


// Пример 3. Получаем имя конкретного Бразильского пользователя. Без какой либо конфигурации
echo '----------------------' . PHP_EOL;
echo \App\User\BrasilUser::findById(rand(10,100))->getFullName() . PHP_EOL;