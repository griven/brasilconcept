#Brasil Concept
Пример с паттерном фабрика, для реализации разной логики в разных проектах.

Для работы namespace необходимо:
```
composer update
```

Использование в консоли:
```
php -f index.php WhateverYouWant
php -f index.php Brasil
php -f index.php England
php -f index.php Russia
```

Message и User разные в зависимости от проекта, либо используется Базовый класс. Вся логика выбора в фабриках.

Order в данном примере всегда один